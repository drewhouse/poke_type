# frozen_string_literal: true

require 'sinatra'
require 'sinatra/json'
require 'json'
require 'alexa_verifier'
require_relative 'app/lib/required_files.rb'

post '/' do
  content_type :json
  AlexaVerifier.valid!(request)
  hash = JSON.parse(request.body.rewind && request.body.read)
  # puts JSON.pretty_generate(request.env)
  ###############################
  puts JSON.pretty_generate(hash)
  RequestHandler.get_response(request: hash['request']).to_json
  # serialized = Pokemon.get_chart_type(type_one, type_two)
  # json serialized
end
