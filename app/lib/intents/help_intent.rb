# frozen_string_literal: true

# When asking for Help
class HelpIntent < BaseIntent
  private

  def set_response
    @response = %( What types do you need help with? )
  end
end
