# frozen_string_literal: true

# Logic for the slot TypeMatchup
class TypeMatchup < DialogIntent
  private

  def set_slots
    @slots = %w[type_one type_two]
  end

  def get_slot_value(name)
    request['intent'].fetch('slots', {})
                     .fetch(name, {})
                     .fetch('value', nil)
  end

  def proceed_results_and_response
    type_one = get_slot_value('type_one')
    type_two = get_slot_value('type_two')
    hash = Pokemon.get_chart_type(type_one, type_two)
    @response = Pokemon::AlexaFormat.format(hash)
  end
end
