# frozen_string_literal: true

# When No intent was found
class FallbackIntent < BaseIntent
end
