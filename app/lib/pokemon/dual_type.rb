require_relative "../pokemon"

module Pokemon
  class DualType
    attr_reader :first, :last, :types, :name,
                :defensive_keywords, :keywords

    def initialize(types, name)
      @first, @last = types
      @name = name
      @defensive_keywords = Pokemon.defensive_keywords
      @keywords = Pokemon::KEYWORDS
      @types = Pokemon.poke_types
      keywords.each do |effectiveness|
        instance_variable_set("@#{effectiveness}", [])
        self.class.send(:attr_accessor, effectiveness)
      end
    end

    def build
      recalculate_modifiers
      self
    end

    private

    def recalculate_modifiers
      types.each do |type|
        #normal
        first_name = @first.find_type_modifier(type.name)
        last_name = @last.find_type_modifier(type.name)
        first_modifier = Pokemon.damage_mapping(first_name)
        last_modifier = Pokemon.damage_mapping(last_name)
        new_modifier = first_modifier * last_modifier
        new_location = Pokemon.modifier_to_name(new_modifier)
        current = self.send(new_location)
        current << type.name
        self.send("#{new_location}=", current)
      end
    end
  end
end
