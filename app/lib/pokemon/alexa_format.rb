# frozen_string_literal: true

module Pokemon
  # results to english
  class AlexaFormat
    def self.format(args)
      @data = args[:data]
      @types_used = @data[:id].split('_')
      # options = %w[long short]
      # @response = send("craft_#{options.sample}_response")
      @response = craft_short_response
    end

    def self.parse_effectiveness
      effectiveness = @data[:attributes]
      type_order = effectiveness.values_at(:quadruple_damage_from,
                                           :double_damage_from,
                                           :neutral_damage_from,
                                           :half_damage_from,
                                           :no_damage_from)
      type_order.find { |type_arr| !type_arr.blank? }
    end

    def self.craft_long_response
      max_effectiveness = parse_effectiveness
      "against #{@types_used.to_sentence} " \
      "#{'type'.pluralize(@types_used.count)}. " \
      'The most effective ' \
      "#{'type'.pluralize(max_effectiveness.count)} " \
      "#{max_effectiveness.count > 1 ? 'are' : 'is'} " \
      "#{parse_effectiveness.to_sentence} "
    end

    def self.craft_short_response
      "Use #{parse_effectiveness.to_sentence} "
    end
  end
end
