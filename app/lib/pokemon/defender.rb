require_relative "type_chart_serializer"
require_relative "dual_type"
require_relative "../pokemon"

module Pokemon
  class Defender
    attr_reader :keywords,
                :poke_types,
                :types,
                :current_types,
                :metadata

    attr_accessor :full_name

    def initialize(*args)
      @keywords = Pokemon.defensive_keywords
      set_instance_vars(args)
      build
    end

    def build
      if current_types.length > 1
        ###Dual Type Logic
        dual = Pokemon::PokeType.combine(current_types, full_name)
        keywords.map do |effectiveness|
          self.send("#{effectiveness}=", dual.send(effectiveness))
        end
      elsif current_types.length == 1
        type = current_types.first
        keywords.map do |effectiveness|
          self.send("#{effectiveness}=", type.send(effectiveness))
        end
      end
    end

    def serialize
      Pokemon::TypeChartSerializer.new(self)
    end

    private

    def set_instance_vars(selected_types)
      @poke_types = Pokemon::PokeType.types
      @current_types = Pokemon::PokeType.find_types(selected_types)
      @full_name = set_full_name
      metadata = Pokemon.defensive_modifiers
      keywords.each do |effectiveness|
        instance_variable_set("@#{effectiveness}", nil)
        self.class.send(:attr_accessor, effectiveness)
      end
    end

    def set_full_name
      self.full_name = current_types.map(&:name).join("_")
    end

    def group_occurences(occurences, effectiveness)
      grouped = {}
      total_occurences = occurences.values.uniq
      total_occurences.each do |num|
        grouped[num] = occurences.select { |k, v| v == num }.keys
      end
      #parse_quads(grouped, effectiveness)
    end
  end
end
