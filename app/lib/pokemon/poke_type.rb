require_relative "../pokemon"

module Pokemon
  class PokeType
    def self.types
      Pokemon.poke_types
    end

    def self.find_types(args)
      types.select { |type| args.include?(type.name) }
    end

    def self.combine(types, name)
      Pokemon::DualType.new(types, name).build
    end

    attr_reader :defensive_keywords,
                :keywords
    attr_accessor :name

    def initialize(data)
      @defensive_keywords = Pokemon.defensive_keywords
      @keywords = Pokemon::KEYWORDS
      @name = nil
      set_instance_vars(data)
    end

    def build
      set_neutral_damage_from
    end

    def find_type_modifier(type)
      defensive_keywords.find do |effectiveness|
        self.send(effectiveness).include?(type)
      end
    end

    private

    def set_instance_vars(args)
      @json = Pokemon.poke_types
      keywords.each do |effectiveness|
        instance_variable_set("@#{effectiveness}", nil)
        self.class.send(:attr_accessor, effectiveness)
      end
      args.each do |key, value|
        self.send("#{key}=", value)
      end
    end

    def set_neutral_damage_from
      type_coverage = []
      defensive_keywords.map do |effectiveness|
        stat = self.send(effectiveness)
        self.send("#{effectiveness}=", (stat || []))
        type_coverage = type_coverage | self.send(effectiveness)
      end
      neutral = Pokemon.get_all_types - type_coverage
      self.neutral_damage_from = neutral
    end
  end
end
