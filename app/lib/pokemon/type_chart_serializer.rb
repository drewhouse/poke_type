# frozen_string_literal: true

require 'fast_jsonapi'
require_relative '../pokemon'
# ##require_relative 'poke_type'

module Pokemon
  # Manipulate the data to read nicely in the api
  class TypeChartSerializer
    include FastJsonapi::ObjectSerializer
    set_type :defender # optional
    set_id :full_name # optional
    attributes :quadruple_damage_from,
               :double_damage_from,
               :neutral_damage_from,
               :half_damage_from,
               :quarter_damage_from,
               :no_damage_from
  end
end
