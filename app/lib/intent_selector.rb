# frozen_string_literal: true

require_relative 'request_handler'

# Selects which intent to use from /intents
class IntentSelector < RequestHandler
  private

  def set_response
    @response = find_intent_class.get_response(request: request)
  end

  def find_intent_class
    begin
      class_name = request['intent']['name'].classify.constantize
    rescue StandardError
      class_name = FallbackIntent
    end
    class_name
  end
end
