# frozen_string_literal: true

require 'json'
require_relative './pokemon/poke_type'

# Main module to house logic for gathering Pokemon Data
module Pokemon
  KEYWORDS = %w[
    double_damage_to
    half_damage_to
    no_damage_to
    quadruple_damage_from
    double_damage_from
    neutral_damage_from
    half_damage_from
    quarter_damage_from
    no_damage_from
  ]

  @types = nil
  @metadata = nil
  @defensive_keywords = nil
  @attack_keywords = nil

  def self.get_chart_type(type1, type2 = nil)
    defender = Pokemon::Defender.new(type1, type2)
    defender.serialize.serializable_hash
  end

  def self.get_json
    path = Dir["#{__dir__}/../static/type_effectiveness.json"]
    file = File.read(path.first)
    json = JSON.parse(file)
    metadata = json['metadata']
    types = json['types'].map do |type|
      self::PokeType.new(type)
    end
    [types, metadata]
  end

  def self.get_defensive_keywords
    KEYWORDS.select do |key, _|
      key.end_with?('_from')
    end
  end

  def self.get_attack_keywords
    KEYWORDS.select do |key, _|
      key.end_with?('_to')
    end
  end

  def self.poke_types
    @types
  end

  def self.defensive_keywords
    @defensive_keywords ||= get_defensive_keywords
  end

  def self.attack_keywords
    @attack_keywords
  end

  def self.metadata
    @metadata
  end

  def self.get_all_types
    poke_types.map(&:name)
  end

  def self.defensive_modifiers
    @metadata['defensive_modifiers']
  end

  def self.damage_mapping(name)
    defensive_modifiers[name]
  end

  def self.modifier_to_name(modifier)
    defensive_modifiers.key(modifier)
  end

  def self.calculate_new_modifier_name(arr)
    modifiers = arr.map { |name| damage_mapping(name) }
    new_modifier = modifiers.inject(:*)
    defensive_modifiers.key(new_modifier)
  end

  def self.format_modifier(name)
    "#{name}_damage_from"
  end

  def self.build
    @types, @metadata = get_json
    @defensive_keywords = get_defensive_keywords
    @attack_keywords = get_attack_keywords
    @types.map(&:build)
  end

  build
end
