# frozen_string_literal: true

require_relative 'pokemon'
require_relative 'intent_selector'
require_relative 'request_handler'
Dir["#{__dir__}/../lib/intents/*.rb"].sort.each { |f| load(f) }
Dir["#{__dir__}/../lib/pokemon/*.rb"].sort.each { |f| load(f) }
